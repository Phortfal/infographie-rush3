#pragma once
#include <vector>
#include <iostream>
#include "Terrain.h"
#include "stdafx.h"

using namespace std;

namespace PM3D
{

	struct Heightmap {
		int x, y;
		float max;
		vector<vector<float>> values;
	};


	class Convertisseur {
	protected:
		Heightmap heightmap;

#pragma region Singleton
	private:
		Convertisseur() = default;
	public:
		static Convertisseur& getInstance() {
			static Convertisseur instance{}; // Guaranteed to be destroyed.
											// Instantiated on first use.
			return instance;
		}
		Convertisseur(Convertisseur const&) = delete;
		void operator=(Convertisseur const&) = delete;
#pragma endregion

	public:
		void LireImage(string filename, CTerrain& terrain, float echelleXY, float echelleZ);
		void LireFichierHeightmap(string filename);
		void ConstruireTerrain(CTerrain& terrain, float echelleXY, float echelleZ);
		void ConstruireIndex(CTerrain& terrain);
		void EnregistrerTout(CTerrain& terrain);
		void EnregistrerTout(CTerrain& terrain, const char* filename);
		void Clear(CTerrain& terrain);
		void ChargerTout(CTerrain& terrain);
		void ChargerTout(CTerrain& terrain, const char* filename);

	private:
		void EnregistrerSommets(CTerrain& terrain, ofstream& out);
		void EnregistrerNormales(CTerrain& terrain, ofstream& out);
		void EnregistrerIndexes(CTerrain& terrain, ofstream& out);
		string loadVertices(CTerrain& terrain, ifstream& myfile, string line);
		string loadNormals(CTerrain& terrain, ifstream& myfile, string line);
		string loadSommets(CTerrain& terrain, ifstream& myfile, string line, int& count);
		void addSommet(CTerrain& terrain, string& line, int& count);
		string ChargerSommets(CTerrain& terrain, ifstream& myfile);
		string ChargerNormales(CTerrain& terrain, ifstream& myfile, string& line);
		string ChargerIndexes(CTerrain& terrain, ifstream& myfile, string& line);
		void ChargerUnIndex(CTerrain& terrain, string& line, const string& delimiter, int& count);
	};

}
#pragma once

using namespace DirectX;

class CCamera
{

public:

	CCamera();



	CCamera(const XMVECTOR& position_in,
		const XMVECTOR& direction_in,
		const XMVECTOR& up_in,
		XMMATRIX* pMatView_in,
		XMMATRIX* pMatProj_in,
		XMMATRIX* pMatViewProj_in);



	const XMVECTOR DEFAULT_UP_VECTOR = XMVectorSet(0.0f, 1.0f, 0.0f, 0.0f);
	const XMVECTOR DEFAULT_DOWN_VECTOR = XMVectorSet(0.0f, -1.0f, 0.0f, 0.0f);

	const XMVECTOR DEFAULT_FORWARD_VECTOR = XMVectorSet(0.0f, 0.0f, 1.0f, 0.0f);
	const XMVECTOR DEFAULT_BACKWARD_VECTOR = XMVectorSet(0.0f, 0.0f, -1.0f, 0.0f);

	const XMVECTOR DEFAULT_LEFT_VECTOR = XMVectorSet(-1.0f, 0.0f, 0.0f, 0.0f);
	const XMVECTOR DEFAULT_RIGHT_VECTOR = XMVectorSet(1.0f, 0.0f, 0.0f, 0.0f);

	XMVECTOR vec_forward, vec_backward, vec_left, vec_right, vec_up, vec_down;

	XMVECTOR position;
	XMVECTOR direction;
	XMVECTOR up;

	XMFLOAT3 pos;
	XMFLOAT3 rot;

	XMMATRIX* pMatView;
	XMMATRIX* pMatProj;
	XMMATRIX* pMatViewProj;

	float angleDirectionCamera;



	void Init(const XMVECTOR& position_in,
		const XMVECTOR& direction_in,
		const XMVECTOR& up_in,
		XMMATRIX* pMatView_in,
		XMMATRIX* pMatProj_in,
		XMMATRIX* pMatViewProj_in);

	void SetPosition(const XMVECTOR& position_in)
	{
		position = position_in;
		XMStoreFloat3(&pos, position);
		Update();
	};

	void SetDirection(const XMVECTOR& direction_in)
	{
		direction = direction_in;
		XMStoreFloat3(&rot, direction);
		Update();
	}

	void SetUp(const XMVECTOR& up_in) /////A REMODIF AVEC LE CONST UP
	{
		up = up_in;
		Update();
	}

	void AdjustRotation(float x, float y, float z)
	{
		rot.x += x;
		rot.y += y;
		rot.z += z;
		direction = XMLoadFloat3(&rot);
	}

	void AdjustRotation(XMVECTOR v)
	{
		XMFLOAT3 tmp;
		XMStoreFloat3(&tmp, v);

		pos.x += tmp.x;
		pos.y += tmp.y;
		pos.z += tmp.z;
		position = XMLoadFloat3(&rot);
	}

	void AdjustPosition(float x, float y, float z)
	{
		pos.x += x;
		pos.y += y;
		pos.z += z;
		position = XMLoadFloat3(&pos);
	}


	void AdjustPosition(XMVECTOR v)
	{
		XMFLOAT3 tmp;
		XMStoreFloat3(&tmp, v);

		pos.x += tmp.x;
		pos.y += tmp.y;
		pos.z += tmp.z;
		position = XMLoadFloat3(&pos);
	}

	void Update()
	{
		XMMATRIX camRotationMatrix = XMMatrixRotationRollPitchYaw(rot.x, rot.y, 0.0f);

		XMVECTOR camTarget = XMVector3TransformCoord(DEFAULT_FORWARD_VECTOR, camRotationMatrix);

		camTarget += position;

		XMVECTOR upDir = XMVector3TransformCoord(DEFAULT_UP_VECTOR, camRotationMatrix);


		// Matrice de la vision
		*pMatView = XMMatrixLookAtLH(position,
			/*(position +  direction)*/ camTarget,
			upDir);

		XMMATRIX vecRotationMatrix = XMMatrixRotationRollPitchYaw(rot.x, rot.y, rot.z);

		vec_forward = XMVector3TransformCoord(DEFAULT_FORWARD_VECTOR, vecRotationMatrix);
		vec_backward = XMVector3TransformCoord(DEFAULT_BACKWARD_VECTOR, vecRotationMatrix);
		vec_left = XMVector3TransformCoord(DEFAULT_LEFT_VECTOR, vecRotationMatrix);
		vec_right = XMVector3TransformCoord(DEFAULT_RIGHT_VECTOR, vecRotationMatrix);
		vec_up = XMVector3TransformCoord(DEFAULT_UP_VECTOR, vecRotationMatrix);
		vec_down = XMVector3TransformCoord(DEFAULT_DOWN_VECTOR, vecRotationMatrix);



		// Recalculer matViewProj 
		*pMatViewProj = (*pMatView) * (*pMatProj);
	}

};
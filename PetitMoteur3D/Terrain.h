#pragma once
#include "d3dx11effect.h"
#include "Objet3D.h"
#include "dispositifD3D11.h"
#include <DirectXMath.h>
#include "sommetbloc.h"
#include "Texture.h"


namespace PM3D
{

	class CTerrain : public CObjet3D {
	public:
		CTerrain(CDispositifD3D11* pDispositif_);

		virtual ~CTerrain();

		virtual void Anime(float tempsEcoule) override;
		virtual void Draw() override;

		void SetTexture(CTexture* pTexture1, CTexture* pTexture2);

	private:
		void InitEffet();

		CDispositifD3D11* pDispositif;

		ID3D11Buffer* pVertexBuffer;
		ID3D11Buffer* pIndexBuffer;

		// Définitions des valeurs d'animation
		ID3D11Buffer* pConstantBuffer;
		XMMATRIX matWorld;
		float rotation;

		// Pour les effets
		ID3DX11Effect* pEffet;
		ID3DX11EffectTechnique* pTechnique;
		ID3DX11EffectPass* pPasse;
		ID3D11InputLayout* pVertexLayout;

		ID3D11ShaderResourceView* pTextureD3D1, *pTextureD3D2;
		ID3D11SamplerState* pSampleState;

		// Les informations pour le terrain
	public:
		int dx, dz, nbSommets, nbPolygones, nbIndexes;
		std::vector<XMFLOAT3> vertices;
		std::vector<XMFLOAT3> normals;

		std::vector<std::pair<int, int>> faces;

		std::vector<CSommetBloc> sommets;
		std::vector<UINT> pIndexes;

		void fillIndexes();

		XMFLOAT3 createNormal(XMFLOAT3 v0, XMFLOAT3 v1, XMFLOAT3 v2);

	public:
		void init(int dx, int dz);
		void init(int dx, int dz, int nbSommets, int nbPolygones, int nbIndexes);
		void creerSommet(float x, float y, float z);
		CSommetBloc& getSommet(int index);
		CSommetBloc& getSommet(int x, int z);

		XMFLOAT3& getVertex(int index);
		XMFLOAT3& getVertex(int x, int z);
		
		XMFLOAT3& getNormal(int index);
		XMFLOAT3& getNormal(int x, int z);

	};

} //	namespace PM3D



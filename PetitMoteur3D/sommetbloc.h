#pragma once

using namespace DirectX;

namespace PM3D
{

	class CSommetBloc
	{
	protected:
		XMFLOAT3 m_Position;
		XMFLOAT3 m_Normal;
		XMFLOAT2 m_CoordTex;

	public:
		XMFLOAT3& getPosition() { return m_Position; }
		XMFLOAT3& getNormal() { return m_Normal; }

		static UINT nbSommet;
		CSommetBloc() = default;
		CSommetBloc(const XMFLOAT3& position);
		//CSommetBloc(const XMFLOAT3& position, const XMFLOAT3& normal);
		CSommetBloc(const XMFLOAT3& position, const XMFLOAT3& normal, const XMFLOAT2& coordTex = XMFLOAT2(0.0f, 0.0f));

		static UINT numElements;
		static D3D11_INPUT_ELEMENT_DESC layout[];

		UINT id;

		
	};

} // namespace PM3D

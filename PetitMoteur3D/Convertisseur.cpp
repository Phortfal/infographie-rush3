#include "stdafx.h"
#include "Convertisseur.h"
#include <string>
#include <string>
#include <algorithm>
#include <fstream>
#include <chrono>
#define DEBUG


namespace PM3D
{

    extern "C" {
#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"
    }


    void Convertisseur::LireImage(string filename, CTerrain& terrain, float echelleXY, float echelleZ)
    {
        LireFichierHeightmap(filename);
        ConstruireTerrain(terrain, echelleXY, echelleZ);
        ConstruireIndex(terrain);
    }

    void Convertisseur::LireFichierHeightmap(string filename)
    {
        // ... x = width, z = height, n = # 8-bit components per pixel ...
        // ... replace '0' with '1'..'4' to force that many components per pixel
        // ... but 'n' will always be the number that it would have been if you said 0
        int x, y, n;
        unsigned char* data = stbi_load(filename.c_str(), &x, &y, &n, 0);
        this->heightmap.x = x;
        this->heightmap.y = y;

        // ... process data if not NULL ..
        this->heightmap.values = vector<vector<float>>(x, vector<float>(y));
        // Utiliser un Lambda
        this->heightmap.max = 0.0f;
        for (int k = 0; k < n; ++k) {
            for (int i = 0; i < x; ++i) {
                for (int j = 0; j < y; ++j) {
                    int src_index = k + n * i + n * x * j;
                    this->heightmap.values.at(i).at(j) = static_cast<float>(data[src_index]) / 255.f;
                    if (this->heightmap.values.at(i).at(j) >= this->heightmap.max) {
                        this->heightmap.max = this->heightmap.values.at(i).at(j);
                    }
                }
            }
        }
        stbi_image_free(data);

        return;
    }

    void Convertisseur::ConstruireTerrain(CTerrain& terrain, float echelleXZ, float echelleY)
    {
        terrain.init(heightmap.x, heightmap.y);

        for (int x = 0; x < terrain.dx; x++) {
            for (int z = 0; z < terrain.dz; z++) {
                terrain.vertices.push_back(XMFLOAT3{ x * echelleXZ, heightmap.values.at(x).at(z) * echelleY, z * echelleXZ });
            }
        }
    }

    void Convertisseur::ConstruireIndex(CTerrain& terrain)
    {
        terrain.fillIndexes();
    }

    void Convertisseur::EnregistrerTout(CTerrain& terrain) {
        EnregistrerTout(terrain, "test.obj");
    }

    void Convertisseur::EnregistrerTout(CTerrain& terrain, const char* filename)
    {
#ifdef DEBUG
        using std::chrono::high_resolution_clock;
        using std::chrono::duration_cast;
        using std::chrono::duration;
        using std::chrono::milliseconds;
#endif // DEBUG

        string v, vn;
        ofstream out{ filename };

#ifdef DEBUG
        auto t1 = high_resolution_clock::now();
#endif // DEBUG

        EnregistrerSommets(terrain, out);
        EnregistrerNormales(terrain, out);

#ifdef DEBUG
        auto t2 = high_resolution_clock::now();
        auto ms_int = duration_cast<milliseconds>(t2 - t1);
        cout << ms_int.count() << "ms\n";
#endif // DEBUG

        out << v;
        out << vn;

#ifdef DEBUG
        t1 = high_resolution_clock::now();
#endif // DEBUG

        EnregistrerIndexes(terrain, out);

#ifdef DEBUG
        t2 = high_resolution_clock::now();
        ms_int = duration_cast<milliseconds>(t2 - t1);
        cout << ms_int.count() << "ms\n";
#endif // DEBUG
    }

    void Convertisseur::EnregistrerSommets(CTerrain& terrain, ofstream& out) {
        std::for_each(terrain.vertices.begin(), terrain.vertices.end(), [&](XMFLOAT3& v) {
            out << "v " << v.x << " " << v.y << " " << v.z << " " << "\n";
        });
    }

    void Convertisseur::EnregistrerNormales(CTerrain& terrain, ofstream& out) {
        std::for_each(terrain.normals.begin(), terrain.normals.end(), [&](XMFLOAT3& n) {
            out << "vn " << n.x << " " << n.y << " " << n.z << " " << "\n";
            });
    }

    void Convertisseur::EnregistrerIndexes(CTerrain& terrain, ofstream& out) {
        auto it = terrain.pIndexes.begin();
        while (it != terrain.pIndexes.end())
        {
            int id1 = 1 + terrain.faces.at(*it).first;
            int idn1 = 1 + terrain.faces.at(*it++).second;

            int id2 = 1 + terrain.faces.at(*it).first;
            int idn2 = 1 + terrain.faces.at(*it++).second;

            int id3 = 1 + terrain.faces.at(*it).first;
            int idn3 = 1 + terrain.faces.at(*it++).second;

            out << "f "
                << id1 << "//" << idn1 << " "
                << id2 << "//" << idn2 << " "
                << id3 << "//" << idn3 << "\n";
        }
    }

    void Convertisseur::Clear(CTerrain& terrain) {
        terrain.dx = {};
        terrain.dz = {};
        terrain.nbPolygones = {};
        terrain.nbSommets = {};
        terrain.nbIndexes = {};
        terrain.sommets = {};
        terrain.pIndexes = {};
    }

    void Convertisseur::ChargerTout(CTerrain& terrain) {
        ChargerTout(terrain, "cube.obj");
    }

    string getToken(const string& line, const string& delimiter) {
        return line.substr(0, line.find(delimiter));
    }

    void eraseToken(string& line, const string& delimiter) {
        line.erase(0, line.find(delimiter) + delimiter.length());
    }

    string popToken(string& line, const string& delimiter) {
        string token = getToken(line, delimiter);
        eraseToken(line, delimiter);
        return token;
    }

    bool isStillInTheSamePartOfTheFile(string& line, const string& delimiter, const string& expectedToken) {
        return expectedToken == getToken(line, delimiter);
    }

    void extractXYZ(string& line, const string& delimiter, float& x, float& y, float& z) {
        string tokenX = popToken(line, delimiter);
        string tokenY = popToken(line, delimiter);
        string tokenZ = popToken(line, delimiter);

        x = stof(tokenX);
        y = stof(tokenY);
        z = stof(tokenZ);
    }

    void extractVertexNormal(string& line, int& indexVertex, int& indexNormal) {
        string tokenVertex = popToken(line, "//");
        string tokenNormal = popToken(line, " ");

        indexVertex = stoi(tokenVertex) - 1;
        indexNormal = stoi(tokenNormal) - 1;
    }

    string getComment(string& line) {
        popToken(line, " ");
        return popToken(line, " ");
    }

    void Convertisseur::ChargerTout(CTerrain& terrain, const char* filename) {
        std::ifstream myfile(filename);

        if (myfile.is_open())
        {
            string line = "init";
            int count = 0;

            while (line != "") {
                while (getToken(line, " ") != "v" && getline(myfile, line)) {
                    // Skip
                }
                line = loadVertices(terrain, myfile, line);

                while (getToken(line, " ") != "vn" && getline(myfile, line)) {
                    // Skip
                }
                line = loadNormals(terrain, myfile, line);

                while (getToken(line, " ") != "f" && getline(myfile, line)) {
                    // Skip
                }
                line = loadSommets(terrain, myfile, line, count);
            }



            myfile.close();
            std::cout << "loaded !" << '\n';
        }

        else std::cout << "unable to open file";

    }


    string Convertisseur::loadVertices(CTerrain& terrain, ifstream& myfile, string line) {
        while (getToken(line, " ") == "v" || (getline(myfile, line) && getToken(line, " ") == "v")) {
            eraseToken(line, " ");

            float x, y, z;
            extractXYZ(line, " ", x, y, z);
            terrain.vertices.push_back(XMFLOAT3{ x, y, z });
        }
        return line;
    }

    string Convertisseur::loadNormals(CTerrain& terrain, ifstream& myfile, string line) {
        while (getToken(line, " ") == "vn" || (getline(myfile, line) && getToken(line, " ") == "vn")) {
            eraseToken(line, " ");

            float x, y, z;
            extractXYZ(line, " ", x, y, z);
            terrain.normals.push_back(XMFLOAT3{ x, y, z });
        }
        return line;
    }

    string Convertisseur::loadSommets(CTerrain& terrain, ifstream& myfile, string line, int& count) {
        while (getToken(line, " ") == "f" || (getline(myfile, line) && getToken(line, " ") == "f")) {
            eraseToken(line, " ");

            addSommet(terrain, line, count);
            addSommet(terrain, line, count);
            addSommet(terrain, line, count);
        }
        return line;
    }

    void Convertisseur::addSommet(CTerrain& terrain, string& line, int& count) {
        int indexVertex, indexNormal;

        extractVertexNormal(line, indexVertex, indexNormal);

        std::pair<int, int> p{ indexVertex, indexNormal };
        // auto finder = std::find(terrain.faces.begin(), terrain.faces.end(), p);

        //if (finder == terrain.faces.end()) {
            terrain.faces.push_back(p);
            terrain.sommets.push_back({ terrain.vertices.at(indexVertex), terrain.normals.at(indexNormal) });
            terrain.pIndexes.push_back(count++);
        //}
        //else {
        //    terrain.pIndexes.push_back(static_cast<int>(std::distance(finder, terrain.faces.begin())));
        //}
    }
}
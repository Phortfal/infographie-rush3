cbuffer param
{
	float4x4 matWorldViewProj;   // la matrice totale 
	float4x4 matWorld;		// matrice de transformation dans le monde 
	float4 vLumiere; 		// la position de la source d'�clairage (Point)
	float4 vCamera; 			// la position de la cam�ra
	float4 vAEcl; 			// la valeur ambiante de l'�clairage
	float4 vAMat; 			// la valeur ambiante du mat�riau
	float4 vDEcl; 			// la valeur diffuse de l'�clairage 
	float4 vDMat; 			// la valeur diffuse du mat�riau 
}

struct VS_Sortie
{
	float4 Pos : SV_Position;
	float3 Norm :    TEXCOORD0;
	float3 vDirLum : TEXCOORD1;
	float3 vDirCam : TEXCOORD2;
	float2 coordTex : TEXCOORD3;
	float3 PosWorld : TEXCOORD4;
};

float seuilY = 30.0f;
float fadeDist = 30.0f;

VS_Sortie MiniPhongVS(float4 Pos : POSITION, float3 Normale : NORMAL, float2 coordTex : TEXCOORD)
{
	VS_Sortie sortie = (VS_Sortie)0;

	sortie.Pos = mul(Pos, matWorldViewProj);
	sortie.Norm = mul(float4(Normale, 0.0f), matWorld).xyz;

	sortie.PosWorld = mul(Pos, matWorld).xyz;

	sortie.vDirLum = vLumiere.xyz - sortie.PosWorld;
	sortie.vDirCam = vCamera.xyz - sortie.PosWorld;

	// Coordonn�es d'application de texture
	sortie.coordTex = coordTex * 100.0f;

	return sortie;
}

Texture2D textureEntreeRock;  // la texture
Texture2D textureEntreeWater;  // la texture
SamplerState SampleState;  // l'�tat de sampling

float4 MiniPhongPS(VS_Sortie vs) : SV_Target
{
	float3 couleur;

	// Normaliser les param�tres
	float3 N = normalize(vs.Norm);
	float3 L = normalize(vs.vDirLum);
	float3 V = normalize(vs.vDirCam);

	// Valeur de la composante diffuse
	float3 diff = saturate(dot(N, L));

	// R = 2 * (N.L) * N � L
	float3 R = normalize(2 * diff * N - L);

	// Puissance de 4 - pour l'exemple
	float S = pow(saturate(dot(R, V)), 4.0f);
	float3 couleurTexture1;
	float3 couleurTexture2;
	float3 couleurTexture;

	float distSeuil1 = vs.PosWorld.y - seuilY;
	distSeuil1 = saturate(distSeuil1 / fadeDist);
	float3 red = { 1.0f, 0.0f, 0.0f };
	float3 blue = { 0.0f, 0.0f, 1.0f };
	float distSeuil2 = seuilY - vs.PosWorld.y + fadeDist;
	distSeuil2 = saturate(distSeuil2 / fadeDist);
	couleurTexture1 = textureEntreeRock.Sample(SampleState, vs.coordTex).rgb;
	//couleurTexture1 = red;
	couleurTexture1 = couleurTexture1 * distSeuil1;
	couleurTexture2 = textureEntreeWater.Sample(SampleState, vs.coordTex).rgb;
	// couleurTexture2 = blue;
	couleurTexture2 = couleurTexture2 * distSeuil2;

	//couleurTexture = couleurTexture1;
	//couleurTexture = couleurTexture2;
	couleurTexture = couleurTexture1 + couleurTexture2;
	
	// I = A + D * N.L + (R.V)n
	couleur = couleurTexture * vAEcl.rgb * vAMat.rgb +
		couleurTexture * vDEcl.rgb * vDMat.rgb * diff;

	couleur += S;

	return float4(couleur, 1.0f);
}

technique11 MiniPhong
{
	pass pass0
	{
		SetVertexShader(CompileShader(vs_5_0, MiniPhongVS()));
		SetPixelShader(CompileShader(ps_5_0, MiniPhongPS()));
		SetGeometryShader(NULL);
	}
}

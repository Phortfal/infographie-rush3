#include "StdAfx.h"

#include "sommetbloc.h"
#include "util.h"
#include "DispositifD3D11.h"

#include "resource.h"
#include "MoteurWindows.h"

#include "Terrain.h"
#include "Convertisseur.h"

namespace PM3D
{

	struct ShadersParams
	{
		XMMATRIX matWorldViewProj;	// la matrice totale 
		XMMATRIX matWorld;			// matrice de transformation dans le monde 
		XMVECTOR vLumiere; 			// la position de la source d'�clairage (Point)
		XMVECTOR vCamera; 			// la position de la cam�ra
		XMVECTOR vAEcl; 			// la valeur ambiante de l'�clairage
		XMVECTOR vAMat; 			// la valeur ambiante du mat�riau
		XMVECTOR vDEcl; 			// la valeur diffuse de l'�clairage 
		XMVECTOR vDMat; 			// la valeur diffuse du mat�riau 
	};

	CTerrain::CTerrain(CDispositifD3D11* pDispositif_)
		: pDispositif(pDispositif_) // Prendre en note le dispositif
		, matWorld(XMMatrixIdentity())
		, rotation(0.0f)
		, pVertexBuffer(nullptr)
		, pIndexBuffer(nullptr)
		, pConstantBuffer(nullptr)
		, pEffet(nullptr)
		, pTechnique(nullptr)
		, pVertexLayout(nullptr)
		, pSampleState(nullptr)
		, pTextureD3D1(nullptr)
		, pTextureD3D2(nullptr)
	{
		Convertisseur::getInstance().LireImage("heightmap-700-700.jpg", *this, 1, 200);
		//Convertisseur::getInstance().ChargerTout(*this, "heightmap.obj");
		//Convertisseur::getInstance().EnregistrerTout(*this, "heightmap.obj");

		// Convertisseur::getInstance().LireImage("hat.png", *this, 1, 200);
	    // Convertisseur::getInstance().ChargerTout(*this, "hat.obj");
		// Convertisseur::getInstance().EnregistrerTout(*this, "hat.obj");
		
		// Cr�ation du vertex buffer et copie des sommets
		ID3D11Device* pD3DDevice = pDispositif->GetD3DDevice();

		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));

		bd.Usage = D3D11_USAGE_IMMUTABLE;
		bd.ByteWidth = (UINT)sizeof(CSommetBloc) * (UINT)sommets.size();
		bd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		bd.CPUAccessFlags = 0;

		D3D11_SUBRESOURCE_DATA InitData;
		ZeroMemory(&InitData, sizeof(InitData));
		InitData.pSysMem = sommets.data();
		pVertexBuffer = nullptr;

		DXEssayer(pD3DDevice->CreateBuffer(&bd, &InitData, &pVertexBuffer), DXE_CREATIONVERTEXBUFFER);

		// Cr�ation de l'index buffer et copie des indices
		ZeroMemory(&bd, sizeof(bd));

		bd.Usage = D3D11_USAGE_IMMUTABLE;
		bd.ByteWidth = (UINT)sizeof(UINT) * (UINT)pIndexes.size();
		bd.BindFlags = D3D11_BIND_INDEX_BUFFER;
		bd.CPUAccessFlags = 0;

		ZeroMemory(&InitData, sizeof(InitData));
		InitData.pSysMem = pIndexes.data();
		pIndexBuffer = nullptr;

		DXEssayer(pD3DDevice->CreateBuffer(&bd, &InitData, &pIndexBuffer), DXE_CREATIONINDEXBUFFER);

		// Initialisation de l'effet
		InitEffet();
	}

	void CTerrain::Anime(float tempsEcoule)
	{
		rotation = rotation + 0 * ((XM_PI * 2.0f) / 10.0f * tempsEcoule);

		// modifier la matrice de l'objet bloc
		matWorld = XMMatrixRotationX(rotation);
	}

	void CTerrain::Draw()
	{
		// Obtenir le contexte
		ID3D11DeviceContext* pImmediateContext = pDispositif->GetImmediateContext();

		// Choisir la topologie des primitives
		pImmediateContext->IASetPrimitiveTopology(D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

		// Source des sommets
		const UINT stride = sizeof(CSommetBloc);
		const UINT offset = 0;
		pImmediateContext->IASetVertexBuffers(0, 1, &pVertexBuffer, &stride, &offset);

		// Source des index
		pImmediateContext->IASetIndexBuffer(pIndexBuffer, DXGI_FORMAT_R32_UINT, 0);

		// input layout des sommets
		pImmediateContext->IASetInputLayout(pVertexLayout);

		// Initialiser et s�lectionner les �constantes� de l'effet
		ShadersParams sp;
		XMMATRIX viewProj = CMoteurWindows::GetInstance().GetMatViewProj();

		sp.matWorldViewProj = XMMatrixTranspose(matWorld * viewProj);
		sp.matWorld = XMMatrixTranspose(matWorld);

		sp.vLumiere = XMVectorSet(-10.0f, 10.0f, -10.0f, 1.0f);
		sp.vCamera = XMVectorSet(0.0f, 0.0f, -10.0f, 1.0f);
		sp.vAEcl = XMVectorSet(0.2f, 0.2f, 0.2f, 1.0f);
		sp.vAMat = XMVectorSet(1.0f, 1.0f, 1.0f, 1.0f);
		sp.vDEcl = XMVectorSet(1.0f, 1.0f, 1.0f, 1.0f);
		sp.vDMat = XMVectorSet(1.0f, 1.0f, 1.0f, 1.0f);
		pImmediateContext->UpdateSubresource(pConstantBuffer, 0, nullptr, &sp, 0, 0);

		ID3DX11EffectConstantBuffer* pCB = pEffet->GetConstantBufferByName("param");  // Nous n'avons qu'un seul CBuffer
		pCB->SetConstantBuffer(pConstantBuffer);

		// Activation de la texture
		ID3DX11EffectShaderResourceVariable* variableTextureRock;
		variableTextureRock = pEffet->GetVariableByName("textureEntreeRock")->AsShaderResource();
		variableTextureRock->SetResource(pTextureD3D1);

		// Activation de la texture
		ID3DX11EffectShaderResourceVariable* variableTextureWater;
		variableTextureWater = pEffet->GetVariableByName("textureEntreeWater")->AsShaderResource();
		variableTextureWater->SetResource(pTextureD3D2);

		// Le sampler state
		ID3DX11EffectSamplerVariable* variableSampler;
		variableSampler = pEffet->GetVariableByName("SampleState")->AsSampler();
		variableSampler->SetSampler(0, pSampleState);

		// **** Rendu de l'objet
		pPasse->Apply(0, pImmediateContext);

		pImmediateContext->DrawIndexed(static_cast<UINT>(pIndexes.size()), 0, 0);
	}

	CTerrain::~CTerrain()
	{
		DXRelacher(pVertexBuffer);
		DXRelacher(pIndexBuffer);
		DXRelacher(pConstantBuffer);
		DXRelacher(pEffet);
		DXRelacher(pVertexLayout);
	}

	void CTerrain::InitEffet()
	{
		// Compilation et chargement du vertex shader
		ID3D11Device* pD3DDevice = pDispositif->GetD3DDevice();

		// Cr�ation d'un tampon pour les constantes du VS
		D3D11_BUFFER_DESC bd;
		ZeroMemory(&bd, sizeof(bd));

		bd.Usage = D3D11_USAGE_DEFAULT;
		bd.ByteWidth = sizeof(ShadersParams);
		bd.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		bd.CPUAccessFlags = 0;
		pD3DDevice->CreateBuffer(&bd, nullptr, &pConstantBuffer);

		// Pour l'effet
		ID3DBlob* pFXBlob = nullptr;

		DXEssayer(D3DCompileFromFile(L"MiniPhong.fx", 0, 0, 0,
			"fx_5_0", 0, 0,
			&pFXBlob, nullptr),
			DXE_ERREURCREATION_FX);

		D3DX11CreateEffectFromMemory(pFXBlob->GetBufferPointer(), pFXBlob->GetBufferSize(), 0, pD3DDevice, &pEffet);

		pFXBlob->Release();

		pTechnique = pEffet->GetTechniqueByIndex(0);
		pPasse = pTechnique->GetPassByIndex(0);

		// Cr�er l'organisation des sommets pour le VS de notre effet
		D3DX11_PASS_SHADER_DESC effectVSDesc;
		pPasse->GetVertexShaderDesc(&effectVSDesc);

		D3DX11_EFFECT_SHADER_DESC effectVSDesc2;
		effectVSDesc.pShaderVariable->GetShaderDesc(effectVSDesc.ShaderIndex, &effectVSDesc2);

		const void* vsCodePtr = effectVSDesc2.pBytecode;
		const unsigned vsCodeLen = effectVSDesc2.BytecodeLength;

		pVertexLayout = nullptr;
		DXEssayer(pD3DDevice->CreateInputLayout(CSommetBloc::layout,
			CSommetBloc::numElements,
			vsCodePtr,
			vsCodeLen,
			&pVertexLayout),
			DXE_CREATIONLAYOUT);

		// Initialisation des param�tres de sampling de la texture
		D3D11_SAMPLER_DESC samplerDesc;

		samplerDesc.Filter = D3D11_FILTER_ANISOTROPIC;
		samplerDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
		samplerDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
		samplerDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
		samplerDesc.MipLODBias = 0.0f;
		samplerDesc.MaxAnisotropy = 4;
		samplerDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
		samplerDesc.BorderColor[0] = 0;
		samplerDesc.BorderColor[1] = 0;
		samplerDesc.BorderColor[2] = 0;
		samplerDesc.BorderColor[3] = 0;
		samplerDesc.MinLOD = 0;
		samplerDesc.MaxLOD = D3D11_FLOAT32_MAX;

		// Cr�ation de l'�tat de sampling
		pD3DDevice->CreateSamplerState(&samplerDesc, &pSampleState);
	}

	void CTerrain::fillIndexes()
	{
		int count = 0;
		for (int x = 0; x < this->dx - 1; x++) {
			for (int z = 0; z < this->dz - 1; z++) {
				//top left triangle
				int index0 = x * dz + z;
				int index1 = x * dz + z + 1;
				int index2 = (x + 1) * dz + z + 1;

				XMFLOAT3 v0 = vertices.at(index0);
				XMFLOAT3 v1 = vertices.at(index1);
				XMFLOAT3 v2 = vertices.at(index2);
				normals.push_back(createNormal(v0, v1, v2));

				sommets.push_back({ v0, normals.back(), XMFLOAT2(static_cast<float>(x) / dx, static_cast<float>(z) / dz) });
				sommets.push_back({ v1, normals.back(), XMFLOAT2(static_cast<float>(x) / dx, static_cast<float>(z+1) / dz) });
				sommets.push_back({ v2, normals.back(), XMFLOAT2(static_cast<float>(x+1) / dx, static_cast<float>(z + 1) / dz) });

				int normalIndex = static_cast<int>(normals.size() - 1);
				faces.push_back({ index0, normalIndex });
				faces.push_back({ index1, normalIndex });
				faces.push_back({ index2, normalIndex });

				pIndexes.push_back(count++);
				pIndexes.push_back(count++);
				pIndexes.push_back(count++);

				//bottom right triangle
				index0 = x * dz + z;
				index1 = (x + 1) * dz + z + 1;
				index2 = (x + 1) * dz + z;

				v0 = vertices.at(index0);
				v1 = vertices.at(index1);
				v2 = vertices.at(index2);
				normals.push_back(createNormal(v0, v1, v2));

				sommets.push_back({ v0, normals.back(), XMFLOAT2(static_cast<float>(x) / dx, static_cast<float>(z) / dz) });
				sommets.push_back({ v1, normals.back(), XMFLOAT2(static_cast<float>(x+1) / dx, static_cast<float>(z+1) / dz) });
				sommets.push_back({ v2, normals.back(), XMFLOAT2(static_cast<float>(x+1) / dx, static_cast<float>(z) / dz) });

				normalIndex = static_cast<int>(normals.size() - 1);
				faces.push_back({ index0, normalIndex });
				faces.push_back({ index1, normalIndex });
				faces.push_back({ index2, normalIndex });

				pIndexes.push_back(count++);
				pIndexes.push_back(count++);
				pIndexes.push_back(count++);
			}
		}
	}

	XMFLOAT3 CTerrain::createNormal(XMFLOAT3 v0, XMFLOAT3 v1, XMFLOAT3 v2) {
		XMVECTOR a = XMLoadFloat3(&v0);
		XMVECTOR b = XMLoadFloat3(&v1);
		XMVECTOR c = XMLoadFloat3(&v2);

		XMVECTOR dir = XMVector3Cross(b - a, c - a);
		XMVector3Normalize(dir);

		XMFLOAT3 result;
		XMStoreFloat3(&result, dir);
		return result;
	}

	void CTerrain::init(int _dx, int _dz)
	{
		dx = _dx;
		dz = _dz;
		nbSommets = dx * dz;
		nbPolygones = 2 * (dx - 1) * (dz - 1);
		nbIndexes = 3 * nbPolygones;
	}

	void CTerrain::init(int _dx, int _dz, int _nbSommets, int _nbPolygones, int _nbIndexes) {
		dx = _dx;
		dz = _dz;
		nbSommets = _nbSommets;
		nbPolygones = _nbPolygones;
		nbIndexes = _nbIndexes;
	}

	void CTerrain::creerSommet(float x, float y, float z)
	{
		CSommetBloc sommet{ XMFLOAT3{x, y, z } };
		sommets.at(sommet.id) = sommet;
	}

	CSommetBloc& CTerrain::getSommet(int index)
	{
		return sommets.at(index);
	}

	CSommetBloc& CTerrain::getSommet(int x, int z) {
		return sommets.at(x * dz + z);
	}

	XMFLOAT3& CTerrain::getVertex(int index)
	{
		return vertices.at(index);
	}

	XMFLOAT3& CTerrain::getVertex(int x, int z) {
		return vertices.at(x * dz + z);
	}

	XMFLOAT3& CTerrain::getNormal(int index)
	{
		return normals.at(index);
	}

	XMFLOAT3& CTerrain::getNormal(int x, int z) {
		return normals.at(x * dz + z);
	}

	void CTerrain::SetTexture(CTexture* pTexture1, CTexture* pTexture2)
	{
		pTextureD3D1 = pTexture1->GetD3DTexture();
		pTextureD3D2 = pTexture2->GetD3DTexture();
	}



} // namespace PM3D
